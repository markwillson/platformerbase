extends Node2D

onready var game = get_node("/root/Game")
onready var player = get_node("../../Player")

export (int) var value
export (int) var score
export (String) var collection_signal
export (AudioStream) var pickup_audiostream

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
func _on_Area2D_body_entered(body):
	if(body.is_in_group("player")):
		Events.emit_signal("coin_collected", [value, score])
#		player.add_money(value)
#		player.emit_signal(collection_signal)
		var audioplayer = get_node("../../FXStreamPlayer")
		audioplayer.stream = pickup_audiostream
		audioplayer.play()
		queue_free()
