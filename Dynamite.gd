extends RigidBody2D

var direction

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func throw():
	apply_impulse(Vector2.ZERO, Vector2(70*direction,-300))
	angular_velocity = 40
	yield(get_tree().create_timer(2.0), "timeout")
	explode()

func explode():
	$Explosion.monitoring = true
	$AnimatedSprite.play("explosion")
	yield(get_tree().create_timer(1.0), "timeout")
	queue_free()

func _on_Explosion_body_entered(body):
	if(body.is_in_group("player")):
		Events.emit_signal("player_got_hit")
	elif(body.is_in_group("dynamite")):
		body.explode()
