extends Node


signal player_got_hit
signal player_died
signal coin_collected(value)
signal player_reached_goal(points)
