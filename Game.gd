extends Node

var levelslist
var current_level_num

var current_level_path
var current_level_node
var next_level_path

# Called when the node enters the scene tree for the first time.
func _ready():
	Events.connect("player_died", self, "restart_level")
	Events.connect("player_reached_goal", self, "player_reached_goal")
	levelslist = load("res://LevelsList.tres")
	current_level_num = 0
	print("Game 'ready' func")

func update_health_ui(playerhealth):
	var ui_node = current_level_node.get_node("Player/UI")
	ui_node.update_player_health(playerhealth)
	
func goto_next_level():
	current_level_num += 1
	get_tree().change_scene_to(levelslist.levels[current_level_num])

func restart_level():
	get_tree().change_scene_to(levelslist.levels[current_level_num])
	
func player_reached_goal(points):
	pass
