extends Node2D

var gainedpoints

# Called when the node enters the scene tree for the first time.
func _ready():
	gainedpoints = 0

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2D_body_entered(body):
	if(body.is_in_group("player")):
		print("player position:", body.global_position.y)
		print("goal position:", global_position.y)
		gainedpoints = abs(round(body.global_position.y - global_position.y))
		Events.emit_signal("player_reached_goal", [gainedpoints])
