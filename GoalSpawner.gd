extends Node2D

export (PackedScene) var goal_scene


# Called when the node enters the scene tree for the first time.
func _ready():
	var goal = goal_scene.instance()
	add_child(goal)
	goal.position = Vector2(0,0)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
