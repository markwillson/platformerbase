extends Area2D

var player_feet_in
var player_body_in

# Called when the node enters the scene tree for the first time.
func _ready():
	player_feet_in = false
	player_body_in = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_Hitbox_body_entered(body):
#	if(body.is_in_group("player")):
#		print("playerpos:", body.global_position.y)
#		print("hitbox pos:", global_position.y)
#		if(body.global_position.y < global_position.y && body.iframes == 0):
#			print("hit top")
#			body.jump()
#			if(is_in_group("spike")):
#				Events.emit_signal("player_got_hit")
#		else:
#			Events.emit_signal("player_got_hit")
	pass


func _on_Hitbox_area_entered(area):
	if(area.is_in_group("player_feet") ||area.is_in_group("player_body") ):
		if(area.is_in_group("player_feet")):
			player_feet_in = true
		if(area.is_in_group("player_body")):
			player_body_in = true
		
		var player = area.get_parent()
		# if we already got hit recently, don't do it again
		if(player.iframes > 0):
			return
		if(player_feet_in && !player_body_in):
			player.jump()
			if(is_in_group("spike")):
				Events.emit_signal("player_got_hit")
			else:
				get_parent().queue_free()
		elif(player_body_in):
			Events.emit_signal("player_got_hit")
