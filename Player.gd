extends KinematicBody2D

const TARGET_FPS = 60
const ACCELERATION = 12
const MAX_SPEED = 64
const FRICTION = 10
const AIR_RESISTANCE = 1
const GRAVITY = 6
const JUMP_FORCE = 200

var motion = Vector2.ZERO

var health
var iframes
var can_control

onready var sprite = $Sprite
onready var animationPlayer = $AnimationPlayer
onready var tween_node = get_node("../Tween")

signal player_got_money
signal player_got_hit

func _ready():
	Events.connect("player_got_hit", self, "take_damage")
	Events.connect("player_reached_goal", self, "reached_goal")
	Events.connect("coin_collected", self, "add_money")
	print("starting scene")
	health = 2
	$UI.update_player_health(health)
	iframes = 0
	can_control = true
	

func _process(delta):
	if(iframes > 0):
		iframes -= 1
		if(iframes % 8 == 0):
			$Sprite.visible = !$Sprite.visible
			
func _physics_process(delta):
	if(!can_control):
		return
		
	var x_input = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	
	if x_input != 0:
		animationPlayer.play("Run")
		motion.x += x_input * ACCELERATION * delta * TARGET_FPS
		motion.x = clamp(motion.x, -MAX_SPEED, MAX_SPEED)
		sprite.flip_h = x_input < 0
	else:
		animationPlayer.play("Stand")
	
	motion.y += GRAVITY * delta * TARGET_FPS
	
	if is_on_floor():
		if x_input == 0:
			motion.x = lerp(motion.x, 0, FRICTION * delta)
			
		if Input.is_action_just_pressed("ui_up"):
			motion.y = -JUMP_FORCE
	else:
		animationPlayer.play("Jump")
		
		if Input.is_action_just_released("ui_up") and motion.y < -JUMP_FORCE/2:
			motion.y = -JUMP_FORCE/2
		
		if x_input == 0:
			motion.x = lerp(motion.x, 0, AIR_RESISTANCE * delta)
	
	motion = move_and_slide(motion, Vector2.UP)

func jump():
	motion.y = -JUMP_FORCE
	
# TO DO: 
# Fill in with what happens when the player gets money!
func add_money(amount):
	pass

func take_damage():
	iframes = 60
	health -= 1
	Game.update_health_ui(health)
	if(health <= 0):
		Game.restart_level()

func reached_goal(points):
	can_control = false
	$AnimationPlayer.play("Grab")
	var goal = get_node("../TileSpawner/GoalSpawner").get_child(0)
	var tween_time = points[0]/100.0
	print(tween_time)
	tween_node.interpolate_property(self, "global_position",
			self.global_position, Vector2(self.global_position.x, goal.global_position.y + 8), tween_time,
			Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween_node.start()
	yield(get_tree().create_timer(tween_time), "timeout")
	
