extends Node2D

export (int) var max_time
var play_time
var player_score
var coins_collected

# Called when the node enters the scene tree for the first time.
func _ready():
	Events.connect("player_reached_goal", self, "player_reached_goal")
	Events.connect("coin_collected", self, "player_coin_collected")
	player_score = 0
	Game.current_level_node = self
	coins_collected = 0
	$Player/UI.update_coins()
	play_time = 0.0
	$Timer.wait_time = max_time
	$Timer.start()
	
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	play_time += delta
	

func player_coin_collected(values):
	coins_collected += values[0]
	player_score += values[1]
	
func player_reached_goal(points):
	player_score += points[0]
	var player_slide_tween_time = points[0]/100.0
	yield(get_tree().create_timer(player_slide_tween_time), "timeout")
	$AnimationPlayer.play("EndGoal")
	var anim_length = $AnimationPlayer.current_animation_length
	yield(get_tree().create_timer(anim_length), "timeout")
	Game.goto_next_level()
