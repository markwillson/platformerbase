extends KinematicBody2D

onready var ledgecheck = $LedgeCheck

var direction = Vector2.LEFT
var velocity = Vector2.ZERO

var stopped

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _physics_process(delta):
	if(stopped):
		return
		
	var found_ledge = !ledgecheck.is_colliding()
	
	if(is_on_wall() || found_ledge):
		direction *= -1
		scale.x *= -1
		
	velocity = direction * 25
	move_and_slide(velocity, Vector2.UP)

