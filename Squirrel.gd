extends "res://Slime.gd"

export (PackedScene) var dynamite_scene


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func on_player_sighted(body):
	if(body.is_in_group("player")):
		stopped = true
		if(body.global_position.x < global_position.x && direction.x == 1):
			direction.x *= -1
			scale.x *= -1
		yield(get_tree().create_timer(.1), "timeout")
		var dynamite = dynamite_scene.instance()
		get_node("../..").add_child(dynamite)
		dynamite.global_position = global_position
		dynamite.direction = direction.x
		dynamite.throw()
		yield(get_tree().create_timer(1.0), "timeout")
		stopped = false
