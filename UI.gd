extends Control

onready var level_timer = get_node("../../Timer")

# Called when the node enters the scene tree for the first time.
func _ready():
	Events.connect("coin_collected", self, "on_coin_collected")

func _process(_delta):
	
#	var time_string = String.append("", ceil(level_timer.time_left))
	$VBoxContainer/TopBar/TimerLabel.bbcode_text = String(ceil(level_timer.time_left))
	
func update_player_health(new_health):
	if(new_health == 1):
		$VBoxContainer/TopBar/Heart2.texture.region.position.x = 16
	elif(new_health == 2):
		$VBoxContainer/TopBar/Heart2.texture.region.position.x = 0

func on_coin_collected(to_add):
	update_coins()
	update_score()
	
func update_coins():
	var new_amt = Game.current_level_node.coins_collected
	$VBoxContainer/HBoxContainer/CoinLabel.bbcode_text = String(new_amt)

func update_score():
	print("updating score")
	var new_score = Game.current_level_node.player_score
	$VBoxContainer/TopBar/Score.bbcode_text = "[center]score:" + String(new_score)
